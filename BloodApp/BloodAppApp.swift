//
//  BloodAppApp.swift
//  BloodApp
//
//  Created by Adrianna Siwik on 31/07/2023.
//

import SwiftUI

@main
struct BloodAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
